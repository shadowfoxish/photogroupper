﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace PhotoGroupper
{
	public class FileInfoDateConverter
	{
		private ConversionMode mode;
		private Regex droidExpression = new Regex(@"^[A-Za-z]{3}_(?<year>[0-9]{4})(?<month>[0-9]{2})(?<day>[0-9]{2})_.*$");

		public FileInfoDateConverter(ConversionMode mode)
		{
			this.mode = mode;
		}


		/// <summary>
		/// Files that fail to convert will be returned with DateTime.MinValue, rather than exceptioning. 
		/// </summary>
		public IEnumerable<DateTime> ConvertBatch(FileInfo[] files)
		{
			foreach (FileInfo file in files)
			{
				DateTime dt;
				try
				{
					dt = Convert(file);
				}
				catch (Exception)
				{
					Console.WriteLine("Ignoring file " + file.Name);
					dt = DateTime.MinValue;
				}
				yield return dt;
			}
		}

		public DateTime Convert(FileInfo file)
		{
			switch (this.mode)
			{
				case ConversionMode.AndroidRegex:
					return GetByAndroidRegex(file.Name);
				case ConversionMode.LastModifiedDate:
					return file.LastWriteTime;
				case ConversionMode.CreationDate:
					return file.CreationTime;
			}
			return new DateTime();
		}

		private DateTime GetByAndroidRegex(string name)
		{
			Match m = droidExpression.Match(name);
			if (m.Success == false)
			{
				throw new Exception("File name not in android format");
			}
			return new DateTime(int.Parse(m.Groups["year"].Value), int.Parse(m.Groups["month"].Value), int.Parse(m.Groups["day"].Value));
		}
	}
}
