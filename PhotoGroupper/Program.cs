﻿using System;
using System.IO;
using System.Linq;

namespace PhotoGroupper
{
	class Program
	{
		static void Main(string[] args)
		{
			//Warning, can  overwrite existing files. Best to do this in a folder 
			//where there arent existing image sub-folders
			Console.WriteLine("Folder Path to process:");
			string path = Console.ReadLine();
			if (!Directory.Exists(path))
			{
				Console.WriteLine("Not valid path.");
				Environment.Exit(1);
			}

			Console.WriteLine("Mode? (A=AndroidRegex, W=LastWriteDate, C=CreatedDate");
			string mode = Console.ReadLine();

			FileInfoDateConverter conv = null;
			if (mode == "A")
			{
				conv = new FileInfoDateConverter(ConversionMode.AndroidRegex);
			}
			else if (mode == "W")
			{
				conv = new FileInfoDateConverter(ConversionMode.LastModifiedDate);
			}
			else if (mode == "C")
			{
				conv = new FileInfoDateConverter(ConversionMode.CreationDate);
			}
			else
			{
				Console.WriteLine("Not valid mode.");
				Environment.Exit(2);
			}

			DirectoryInfo folder = new DirectoryInfo(path);

			FileInfo[] files = folder.GetFiles();

			string[] dateGroups = conv.ConvertBatch(files)
				.Except(new DateTime[] { DateTime.MinValue })
				.Select(f => f.ToString("yyyy-MM-dd"))
				.Distinct().ToArray();

			foreach (string dateGroup in dateGroups)
			{
				Directory.CreateDirectory(Path.Combine(path, dateGroup));
			}
			int count = 0;
			foreach (var file in files)
			{
				try
				{
					string destinationDateFolderName = conv.Convert(file).ToString("yyyy-MM-dd");
					string destinationPath = Path.Combine(path, destinationDateFolderName, file.Name);

					Console.WriteLine($"Moving file {file.Name} to {destinationPath}");
					file.MoveTo(destinationPath);
					count++;
				} 
				catch(Exception ex)
				{
					Console.WriteLine($"Error: {file.Name} : {ex.Message}");
				}
			}

			Console.WriteLine($"Moved {count} files");
		}
	}
}
