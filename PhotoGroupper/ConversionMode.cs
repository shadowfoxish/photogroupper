﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotoGroupper
{
	public enum ConversionMode : int
	{
		LastModifiedDate = 1,
		CreationDate = 2,
		AndroidRegex = 3,
	}
}
