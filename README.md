This program takes a directory of files, and moves those files into dated sub-folders.

Currently can do this by Last Modified Time, Creation Time, or by parsing the file name stored in android camera format. (IMG_20190902_123456789.jpg)